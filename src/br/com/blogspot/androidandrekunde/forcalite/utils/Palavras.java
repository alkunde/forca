package br.com.blogspot.androidandrekunde.forcalite.utils;

import java.util.Random;

public class Palavras {

    private String[] palavras = new String[] {"AVI�O", "AERONAVE", "AUTOM�VEL", "MOTOCICLETA",
            "BICICLETA", "IATE", "NAVIO", "TERRA", "MERC�RIO", "PLUT�O", "MARTE", "JUPTER", "NETUNO",
            "ELEFANTE", "ESCORPI�O", "RINOCERONTE", "DINOSSAURO", "REFRIGERADOR", "LIQUIDIFICADOR",
            "TELEVISOR", "POLTRONA", "SECADORA", "ESCORREDOR", "BRASIL", "�FRICA", "EUROPA", "AMSTERD�", "ESTADOS UNIDOS",
            "GR�CIA", "ARGENTINA", "VENEZUELA", "BOTAFOGO", "S�O PAULO", "FLAMENGO", "PALMEIRAS", "FLUMINENSE", "AMOR",
            "INTELECTUAL", "S�BIO", "CULTURA", "SABEDORIA", "TUCANO", "BEIJA-FLOR", "ZEBRA", "CRUZEIRO", "COMPUTADOR",
            "FACULDADE", "PIPOCA", "MACARR�O", "FEIJOADA", "SAB�O EM P�", "LAVANDERIA", "COZINHA", "CHURRASCO",
            "CATARINENSE", "AFRICANO", "BRASILEIRO", "AMERICANO", "GA�CHO", "PARANAENSE", "MINEIRO", "SANTISTA",
            "LIXEIRO", "PROGRAMADOR", "LUMIN�RIA", "LUTADOR", "COZINHEIRO", "CARTEIRO", "VENDEDOR", "FLORICULTURA",
            "JAP�O", "AR�BIA SAUDITA", "EQUADOR", "M�XICO", "PORTUGUAL", "ALEMANHA", "PROFESSOR", "CHAVEIRO", "DOCUMENTOS",
            "DOCUMENT�RIO", "FAM�LIA", "FAMILIARES", "LANCHONETE"};

    public Palavras() {
    }

    public String sorteio() {
        String palavraSorteada = palavras[(int)(random()*palavras.length)];

        return palavraSorteada;
    }

    public static double random() {
        Random r = new Random();

        return r.nextDouble();
    }
}
